package dmitriy.vkphotos;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dmitriy on 10.02.2017.
 */

public class GridViewAdapter extends ArrayAdapter<Photo> {
    private static final int COUNT = 100;
    private Context context;
    private int layoutResourceId;
    private List<Photo> photos;
    private ProgressBar progressBarGridView;
    private boolean isLoading = false;
    private boolean isAllDownloaded = false;
    private int radius;
    private int offset;

    public GridViewAdapter(Context context, int layoutResourceId, List<Photo> photos, ProgressBar progressBarGridView) {
        super(context, layoutResourceId, photos);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.progressBarGridView = progressBarGridView;
        this.photos = photos;
        SharedPreferences sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        radius = Integer.parseInt(sharedPreferences.getString("radius", "100"));
        offset = 0;
    }

    public boolean isAllDownloaded() {
        return isAllDownloaded;
    }

    public void setAllDownloaded(boolean allDownloaded) {
        isAllDownloaded = allDownloaded;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public void setGridData(List<Photo> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) row.findViewById(R.id.gridItemTitle);
            holder.imageView = (ImageView) row.findViewById(R.id.gridItemImage);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        Photo photo = photos.get(position);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, dd MMMMM yyyy, HH:mm:ss", Locale.getDefault());
        holder.titleTextView.setText(simpleDateFormat.format(new Date(photo.getDate() * 1000)));
        Picasso.with(context).load(getTheWorstPhoto(photo)).into(holder.imageView);
        return row;
    }


    private String getTheWorstPhoto(Photo photo) {
        //if (photo.getPhoto75() != null) return photo.getPhoto75();
        if (photo.getPhoto130() != null) return photo.getPhoto130();
        else if (photo.getPhoto604() != null) return photo.getPhoto604();
        else if (photo.getPhoto807() != null) return photo.getPhoto807();
        else if (photo.getPhoto1280() != null) return photo.getPhoto1280();
        else if (photo.getPhoto2560() != null) return photo.getPhoto2560();
        else return null;
    }

    private List<Photo> download(double lat, double lng) throws IOException {
        List<Photo> photos = new ArrayList<>();
        URL url = new URL("https://api.vk.com/method/photos.search?lat=" + lat + "&long=" + lng + "&count=" + COUNT + "&offset=" + offset + "&radius=" + radius + "&v=5.62");
        URLConnection urlConnection = url.openConnection();
        InputStreamReader inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
        JsonReader jsonReader = new JsonReader(inputStreamReader);
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObjectResponse = jsonParser.parse(jsonReader).getAsJsonObject();
        JsonObject jsonObject = jsonObjectResponse.get("response").getAsJsonObject();
        JsonArray jsonArray = jsonObject.getAsJsonArray("items");//только публичные(без токена)
        int size = jsonObject.getAsJsonPrimitive("count").getAsInt();//всего и публичных,и закрытых.
        //System.out.println("count: " + count);
        //System.out.println("size: " + size);
        //System.out.println("jsonArray: " + jsonArray.size());
        System.out.println("Смещение: " + offset);
        System.out.println("Вернулось в массиве: " + jsonArray.size());
        offset += jsonArray.size();
        for (int i = 0; i < Math.min(COUNT, jsonArray.size()); i++) {
            Integer id = jsonArray.get(i).getAsJsonObject().get("id") == null ? null : jsonArray.get(i).getAsJsonObject().get("id").getAsInt();
            Integer albumId = jsonArray.get(i).getAsJsonObject().get("album_id") == null ? null : jsonArray.get(i).getAsJsonObject().get("album_id").getAsInt();
            Integer ownerId = jsonArray.get(i).getAsJsonObject().get("owner_id") == null ? null : jsonArray.get(i).getAsJsonObject().get("owner_id").getAsInt();
            Integer userId = jsonArray.get(i).getAsJsonObject().get("user_id") == null ? null : jsonArray.get(i).getAsJsonObject().get("user_id").getAsInt();
            String text = jsonArray.get(i).getAsJsonObject().get("text") == null ? null : jsonArray.get(i).getAsJsonObject().get("text").getAsString();
            Long date = jsonArray.get(i).getAsJsonObject().get("date") == null ? null : jsonArray.get(i).getAsJsonObject().get("date").getAsLong();
            String photo75 = jsonArray.get(i).getAsJsonObject().get("photo_75") == null ? null : jsonArray.get(i).getAsJsonObject().get("photo_75").getAsString();
            String photo130 = jsonArray.get(i).getAsJsonObject().get("photo_130") == null ? null : jsonArray.get(i).getAsJsonObject().get("photo_130").getAsString();
            String photo604 = jsonArray.get(i).getAsJsonObject().get("photo_604") == null ? null : jsonArray.get(i).getAsJsonObject().get("photo_604").getAsString();
            String photo807 = jsonArray.get(i).getAsJsonObject().get("photo_807") == null ? null : jsonArray.get(i).getAsJsonObject().get("photo_807").getAsString();
            String photo1280 = jsonArray.get(i).getAsJsonObject().get("photo_1280") == null ? null : jsonArray.get(i).getAsJsonObject().get("photo_1280").getAsString();
            String photo2560 = jsonArray.get(i).getAsJsonObject().get("photo_2560") == null ? null : jsonArray.get(i).getAsJsonObject().get("photo_2560").getAsString();
            Integer width = jsonArray.get(i).getAsJsonObject().get("width") == null ? null : jsonArray.get(i).getAsJsonObject().get("width").getAsInt();
            Integer height = jsonArray.get(i).getAsJsonObject().get("height") == null ? null : jsonArray.get(i).getAsJsonObject().get("height").getAsInt();
            Integer postId = jsonArray.get(i).getAsJsonObject().get("post_id") == null ? null : jsonArray.get(i).getAsJsonObject().get("post_id").getAsInt();
            Double longitude = jsonArray.get(i).getAsJsonObject().get("long") == null ? null : jsonArray.get(i).getAsJsonObject().get("long").getAsDouble();
            Double latitude = jsonArray.get(i).getAsJsonObject().get("lat") == null ? null : jsonArray.get(i).getAsJsonObject().get("lat").getAsDouble();
            Photo photo = new Photo(id, albumId, ownerId, userId, text, date, photo75, photo130, photo604, photo807, photo1280, photo2560, width, height, postId, longitude, latitude);
            //костыль.Т.к. api вк возвращает повторяющиеся фотки...
            boolean contains = false;
            for (Photo p : this.photos) {
                if (p.getId().equals(photo.getId())) {
                    contains = true;
                    break;
                }
            }
            if (!contains) photos.add(photo);
            //
        }
        inputStreamReader.close();
        jsonReader.close();
        return photos;
    }

    public void downloadPhotos() {
        new AsyncHttpTask(this).execute();
    }

    private Point geoCoding(String address) throws IOException, JSONException {
        Point point = null;
        URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(address, "UTF-8"));
        URLConnection urlConnection = url.openConnection();
        InputStreamReader inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
        JsonReader jsonReader = new JsonReader(inputStreamReader);
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(jsonReader).getAsJsonObject();
        String status = jsonObject.get("status").getAsString();
        if (status.equals("OK")) {
            JsonArray results = jsonObject.get("results").getAsJsonArray();
            double lng = results.get(0).getAsJsonObject().get("geometry").getAsJsonObject().get("location").getAsJsonObject().get("lng").getAsDouble();
            double lat = results.get(0).getAsJsonObject().get("geometry").getAsJsonObject().get("location").getAsJsonObject().get("lat").getAsDouble();
            inputStreamReader.close();
            jsonReader.close();
            point = new Point(lat, lng);
        }
        return point;
    }

    static class ViewHolder {
        TextView titleTextView;
        ImageView imageView;
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<Object, Object, List<Photo>> {
        private GridViewAdapter gridViewAdapter;

        public AsyncHttpTask(GridViewAdapter gridViewAdapter) {
            this.gridViewAdapter = gridViewAdapter;
        }

        @Override
        protected void onPostExecute(List<Photo> photos) {
            if (photos == null || photos.size() == 0) {
                setAllDownloaded(true);
            } else {
                gridViewAdapter.addAll(photos);
                System.out.println("Сейчас в массиве: " + gridViewAdapter.getCount());
                //notifyDataSetChanged();
            }
            setLoading(false);
            progressBarGridView.setVisibility(View.GONE);
        }

        @Override
        protected void onPreExecute() {
            progressBarGridView.setVisibility(View.VISIBLE);
            setLoading(true);
        }

        @Override
        protected List<Photo> doInBackground(Object[] params) {
            List<Photo> photos = null;
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
                String address = sharedPreferences.getString("address", "Самара");
                Point point = geoCoding(address);
                if (point != null)
                    photos = download(point.latitude, point.longitude);//подгрузка по 100
                else photos = download(0, 0);//подгрузка по 100
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return photos;
        }
    }

    class Point {
        double latitude;
        double longitude;

        public Point(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

}
