package dmitriy.vkphotos;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Locale;

import uk.co.senab.photoview.PhotoView;

/**
 * Created by Dmitriy on 11.02.2017.
 */

public class DetailsActivity extends ActionBarActivity {
    private TextView titleTextView;
    private PhotoView photoView;
    private ProgressBar progressBarDetailsView;
    private Button buttonInfo;
    private Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_view_activity);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        photo = (Photo) getIntent().getSerializableExtra("photo");
        titleTextView = (TextView) findViewById(R.id.title);
        photoView = (PhotoView) findViewById(R.id.gridItemImage);
        buttonInfo = (Button) findViewById(R.id.buttonInfo);
        buttonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsActivity.this, InfoActivity.class);
                intent.putExtra("photo", photo);
                //Start details activity
                startActivity(intent);
            }
        });
        progressBarDetailsView = (ProgressBar) findViewById(R.id.progressBarDetailsView);
        photoView.setMaximumScale(6.0f);
        photoView.setMediumScale(3.0f);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, dd MMMMM yyyy, HH:mm:ss", Locale.getDefault());
        titleTextView.append(simpleDateFormat.format(photo.getDate() * 1000));
        progressBarDetailsView.setVisibility(View.VISIBLE);
        Picasso.with(this).load(getTheBestPhoto(photo)).into(photoView, new Callback() {
            @Override
            public void onSuccess() {
                progressBarDetailsView.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progressBarDetailsView.setVisibility(View.GONE);
            }
        });
        new AsyncHttpTask().execute();
    }

    public String getTheBestPhoto(Photo photo) {
        //if (photo.getPhoto2560() != null) return photo.getPhoto2560();
        if (photo.getPhoto1280() != null) return photo.getPhoto1280();
        else if (photo.getPhoto807() != null) return photo.getPhoto807();
        else if (photo.getPhoto604() != null) return photo.getPhoto604();
        else if (photo.getPhoto130() != null) return photo.getPhoto130();
        else if (photo.getPhoto75() != null) return photo.getPhoto75();
        else return null;
    }

    public class AsyncHttpTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPostExecute(String string) {
            titleTextView.append("\n" + string);
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + photo.getLatitude() + "," + photo.getLongitude() + "&language=ru&sensor=false");
                URLConnection urlConnection = url.openConnection();
                InputStreamReader inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
                JsonReader jsonReader = new JsonReader(inputStreamReader);
                JsonParser jsonParser = new JsonParser();
                JsonArray jsonArray = jsonParser.parse(jsonReader).getAsJsonObject().get("results").getAsJsonArray();
                if (jsonArray.size() > 0) {
                    result = jsonArray.get(0).getAsJsonObject().get("formatted_address").getAsString();
                }
                inputStreamReader.close();
                jsonReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

}
