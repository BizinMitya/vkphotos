package dmitriy.vkphotos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import java.util.ArrayList;

public class GridViewActivity extends ActionBarActivity {
    private GridView gridView;
    private ProgressBar progressBarGridView;
    private GridViewAdapter gridViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_view_activity);
        gridView = (GridView) findViewById(R.id.gridView);
        progressBarGridView = (ProgressBar) findViewById(R.id.progressBarGridView);
        gridViewAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, new ArrayList<Photo>(), progressBarGridView);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnScrollListener(new GridView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && !gridViewAdapter.isLoading() && !gridViewAdapter.isAllDownloaded()) {
                    gridViewAdapter.downloadPhotos();
                }
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //Get item at position
                Photo photo = (Photo) parent.getItemAtPosition(position);
                //Pass the image title and url to DetailsActivity
                Intent intent = new Intent(GridViewActivity.this, DetailsActivity.class);
                intent.putExtra("photo", photo);
                //Start details activity
                startActivity(intent);
            }
        });
    }
}
