package dmitriy.vkphotos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Dmitriy on 12.02.2017.
 */

public class MainActivity extends Activity {
    private SharedPreferences sharedPreferences;
    private TextView textViewSearch;
    private TextView textViewSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        sharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        if (!sharedPreferences.contains("radius")) {
            sharedPreferences.edit().putString("radius", "100").apply();
        }
        if (!sharedPreferences.contains("address")) {
            sharedPreferences.edit().putString("address", "Самара").apply();
        }
        textViewSearch = (TextView) findViewById(R.id.textViewSearch);
        textViewSettings = (TextView) findViewById(R.id.textViewSettings);
        textViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GridViewActivity.class);
                startActivity(intent);
            }
        });
        textViewSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
