package dmitriy.vkphotos;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Dmitriy on 12.02.2017.
 */

public class InfoActivity extends Activity {
    private Photo photo;
    private LinearLayout infoLayout;
    private TextView textView;
    private TextView addressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_activity);
        photo = (Photo) getIntent().getSerializableExtra("photo");
        infoLayout = (LinearLayout) findViewById(R.id.infoLayout);
        textView = new TextView(this);
        if (photo.getUserId() != null) {
            if (photo.getUserId().equals(100)) {//от имени сообщества
                textView.append("Сообщество:\nhttps://vk.com/club" + (-photo.getOwnerId()) + "\n");
            } else {//от имени пользователя в сообществе
                textView.append("Пользователь:\nhttps://vk.com/id" + photo.getUserId() + "\n");
                textView.append("Сообщество:\nhttps://vk.com/club" + (-photo.getOwnerId()) + "\n");
            }
        } else {
            textView.append("Пользователь:\nhttps://vk.com/id" + photo.getOwnerId() + "\n");
        }
        if (photo.getPostId() != null) {
            textView.append("Пост:\nhttps://vk.com/wall" + photo.getOwnerId() + "_" + photo.getPostId() + "\n");
        }
        if (!photo.getText().equals(""))
            textView.append("Текст к фотографии:\n" + photo.getText() + "\n");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, dd MMMMM yyyy, HH:mm:ss", Locale.getDefault());
        textView.append("Дата загрузки фото:\n" + simpleDateFormat.format(photo.getDate() * 1000) + "\n");
        addressTextView = new TextView(this);
        new AsyncHttpTask(this).execute();
        if (photo.getPhoto75() != null)
            textView.append("URL фото с максимальным размером 75x75px:\n" + photo.getPhoto75() + "\n");
        if (photo.getPhoto130() != null)
            textView.append("URL фото с максимальным размером 130x130px:\n" + photo.getPhoto130() + "\n");
        if (photo.getPhoto604() != null)
            textView.append("URL фото с максимальным размером 604x604px:\n" + photo.getPhoto604() + "\n");
        if (photo.getPhoto807() != null)
            textView.append("URL фото с максимальным размером 807x807px:\n" + photo.getPhoto807() + "\n");
        if (photo.getPhoto1280() != null)
            textView.append("URL фото с максимальным размером 1280x1280px:\n" + photo.getPhoto1280() + "\n");
        if (photo.getPhoto2560() != null)
            textView.append("URL фото с максимальным размером 2560x2560px:\n" + photo.getPhoto2560() + "\n");
        textView.append("Адрес:");
        textView.setTextColor(Color.BLACK);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextSize(16);
        textView.setTextIsSelectable(true);
        Intent test = new Intent(Intent.ACTION_VIEW, Uri.parse("https://yandex.ru"));
        if (this.getPackageManager().resolveActivity(test, 0) != null) {
            Linkify.addLinks(textView, Linkify.WEB_URLS);
        }
        addressTextView.setTypeface(Typeface.DEFAULT_BOLD);
        addressTextView.setTextSize(16);
        infoLayout.addView(textView);
    }

    public class AsyncHttpTask extends AsyncTask<Void, Void, String> {
        private Activity activity;

        public AsyncHttpTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPostExecute(String string) {
            String uri = String.format(Locale.ENGLISH, "geo:%f,%f", photo.getLatitude(), photo.getLongitude());
            Intent test = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            if (activity.getPackageManager().resolveActivity(test, 0) != null) {
                Pattern pattern = Pattern.compile(".*", Pattern.DOTALL);
                addressTextView.append(string);
                Linkify.addLinks(addressTextView, pattern, "geo:0,0?q=");
                infoLayout.addView(addressTextView);
            } else {
                addressTextView.append(string);
                addressTextView.setTextColor(Color.BLACK);
                addressTextView.setTextIsSelectable(true);
                infoLayout.addView(addressTextView);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + photo.getLatitude() + "," + photo.getLongitude() + "&language=ru&sensor=false");
                URLConnection urlConnection = url.openConnection();
                InputStreamReader inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
                JsonReader jsonReader = new JsonReader(inputStreamReader);
                JsonParser jsonParser = new JsonParser();
                JsonArray jsonArray = jsonParser.parse(jsonReader).getAsJsonObject().get("results").getAsJsonArray();
                if (jsonArray.size() > 0) {
                    result = jsonArray.get(0).getAsJsonObject().get("formatted_address").getAsString();
                }
                inputStreamReader.close();
                jsonReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }
}
