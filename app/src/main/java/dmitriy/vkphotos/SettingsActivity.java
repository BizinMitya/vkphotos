package dmitriy.vkphotos;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Dmitriy on 12.02.2017.
 */

public class SettingsActivity extends Activity {
    private EditText editTextAddress;
    private EditText editTextRadius;
    private Button buttonSave;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        editTextAddress.setText(sharedPreferences.getString("address", "Самара"));
        editTextRadius = (EditText) findViewById(R.id.editTextRadius);
        editTextRadius.setText(sharedPreferences.getString("radius", "100"));
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putString("address", String.valueOf(editTextAddress.getText())).apply();
                try {
                    Integer.parseInt(String.valueOf(editTextRadius.getText()));
                    sharedPreferences.edit().putString("radius", String.valueOf(editTextRadius.getText())).apply();
                } catch (NumberFormatException e) {

                }
            }
        });
    }
}
