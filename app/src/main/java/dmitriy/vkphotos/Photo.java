package dmitriy.vkphotos;

import java.io.Serializable;

/**
 * Created by Dmitriy on 10.02.2017.
 */

public class Photo implements Serializable {
    private Integer id;//идентификатор фотографии
    private Integer albumId;//идентификатор альбома, в котором находится фотография
    private Integer ownerId;//идентификатор владельца фотографии
    private Integer userId;//идентификатор пользователя, загрузившего фото (если фотография размещена в сообществе). Для фотографий, размещенных от имени сообщества, user_id = 100
    private String text;//текст описания фотографии
    private Long date;//дата добавления в формате Unixtime
    private String photo75;//URL копии фотографии с максимальным размером 75x75px
    private String photo130;//URL копии фотографии с максимальным размером 130x130px
    private String photo604;//URL копии фотографии с максимальным размером 604x604px
    private String photo807;//URL копии фотографии с максимальным размером 807x807px
    private String photo1280;//URL копии фотографии с максимальным размером 1280x1024px
    private String photo2560;//URL копии фотографии с максимальным размером 2560x2048px
    private Integer width;//ширина оригинала фотографии в пикселах
    private Integer height;//высота оригинала фотографии в пикселах
    private Integer postId;
    private Double longitude;
    private Double latitude;

    public Photo(Integer id, Integer albumId, Integer ownerId, Integer userId, String text, Long date, String photo75, String photo130, String photo604, String photo807, String photo1280, String photo2560, Integer width, Integer height, Integer postId, Double longitude, Double latitude) {
        this.id = id;
        this.albumId = albumId;
        this.ownerId = ownerId;
        this.userId = userId;
        this.text = text;
        this.date = date;
        this.photo75 = photo75;
        this.photo130 = photo130;
        this.photo604 = photo604;
        this.photo807 = photo807;
        this.photo1280 = photo1280;
        this.photo2560 = photo2560;
        this.width = width;
        this.height = height;
        this.postId = postId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", albumId=" + albumId +
                ", ownerId=" + ownerId +
                ", userId=" + userId +
                ", text='" + text + '\'' +
                ", date=" + date +
                ", photo75='" + photo75 + '\'' +
                ", photo130='" + photo130 + '\'' +
                ", photo604='" + photo604 + '\'' +
                ", photo807='" + photo807 + '\'' +
                ", photo1280='" + photo1280 + '\'' +
                ", photo2560='" + photo2560 + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", postId=" + postId +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getPhoto75() {
        return photo75;
    }

    public void setPhoto75(String photo75) {
        this.photo75 = photo75;
    }

    public String getPhoto130() {
        return photo130;
    }

    public void setPhoto130(String photo130) {
        this.photo130 = photo130;
    }

    public String getPhoto604() {
        return photo604;
    }

    public void setPhoto604(String photo604) {
        this.photo604 = photo604;
    }

    public String getPhoto807() {
        return photo807;
    }

    public void setPhoto807(String photo807) {
        this.photo807 = photo807;
    }

    public String getPhoto1280() {
        return photo1280;
    }

    public void setPhoto1280(String photo1280) {
        this.photo1280 = photo1280;
    }

    public String getPhoto2560() {
        return photo2560;
    }

    public void setPhoto2560(String photo2560) {
        this.photo2560 = photo2560;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
